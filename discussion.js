// Query Operator

db.inventory.insertMany([
    {
        "name": "JavaScript for Beginners",
        "author": "James Doe",
        "price": 5000,
        "stocks": 50,
        "publisher": "JS Publishing House"
    },
    {
        "name": "HTML and CSS",
        "author": "John Tomas",
        "price": 2600,
        "stocks": 38,
        "publisher": "NY Publishers"
    },
    {
        "name": "Web Development Fundamentals",
        "author": "Noah Jimenez",
        "price": 3000,
        "stocks": 10,
        "publisher": "Big Idea Publishing"
    },
    {
        "name": "Java Programming",
        "author": "David Michael",
        "price": 10000,
        "stocks": 100,
        "publisher": "JS Publishing House"
    }
]);

    db.inventory.updateOne(
        {"name": "HTML and CSS"},
        {
            $set: {
                "price": 2500
            }
        }
    );

// Comparison Query Operators

// $gt / $gte operator (greater than or greater than equal)
/*
    Syntax:
        db.collectionName.find({"field": { $gt: value }});
        db.collectionName.find({"field": { $gte: value }});
*/

    db.inventory.find({
        "stocks": {
            $gt: 50
        }
    });

    db.inventory.find({
        "stocks": {
            $gte: 50 
        }
    });


// $lt / $lte Operator (less than or less than equal)
/*
    Syntax:
        db.collectionName.find({"field": { $lt: value }});
        db.collectionName.find({"field": { $lte: value }});
*/

    db.inventory.find({
        "stocks": {
            $lt: 50 
        }
    });

    db.inventory.find({
        "stocks": {
            $lte: 50 
        }
     });


// $ne operator (not equal)
/*
      Syntax:
        db.collectionName.find({"field": { $ne: value }});
*/

     db.inventory.find({
        "stocks": {
            $ne: 50
        }
     });

// $eq operator (equal)
/**
     Syntax:
        db.collectionName.find({"field": { $eq: value }});
 */

    db.inventory.find({
        "stocks": {
            $qe: 50
        }
    });

// $in operator (in)
/*
    Syntax:
        db.collectionName.find({"field": { $in: [ value1, value2 ] }});
*/

    db.inventory.find({
        "price": {
            $in: [10000, 5000]
        }
    });


// $nin operator (not in)
/*
      Syntax:
        db.collectionName.find({ "field": { $nin: [ value1, value2 ] }});
*/

    db.inventory.find({
        "price": {
            $nin: [10000, 5000]
        }
    });

    db.inventory.find({
        "price": {$gt: 2000} && {$lt: 4000}
    });


// MIni Activity    
    db.inventory.find({
        "price": {
            $lte: [4000]
        }
    });


    db.inventory.find({
        "stocks": {
            $in: [50, 100]
        }
    });

// LOGICAL QUERY OPERATORS

// $or Operator
/**
        Syntax:
            db.collectionName.find({ 
                $or: [
                    { fieldA: valueA },
                    { fieldB: ValueB}
                ]
            });
 */

    db.inventory.find({
        $or: [
            { "name": "HTML and CSS"},
            { "publisher": "JS Publishing House"}
        ]
    });

    db.inventory.find({
        $or: [
            { "author": "James Doe" },
            {"price": {
                $lte: 5000
            }}
        ]
    });

// $and operator
/**
        Syntax:
            db.collectionName.find({ 
                $and: [
                    { fieldA: valueA },
                    { fieldB: ValueB}
                ]
            });
 */

    db.inventory.find({
        $and: [
            {"stocks": {
                $ne: 50
            }},
            {
                "price": {
                    $ne: 5000
                }
            }
        ]
    });


// by default (same result to $and operator)
    db.inventory.find({
       "stocks": {
            $ne: 50
            },
        "price": {
            $ne: 5000
            }
    });


// Field Projection
/*
    Inclusion (kung anu lang ang gusto mong ire'reveal na details)

        Syntax:
            db.collectionName.find({criteria}, {field: 1});
*/

        db.inventory.find(
        {
            "publisher": "JS Publishing House"
        },
        {
            "name": 1,
            "author": 1,
            "price": 1
        }
    );

// EXCLUSION - (ihahide yung details na iproprovide mo)
/*
       Syntax:
            db.collectionName.find({criteria}, {field: 0});
*/

    db.inventory.find(
        {
            "author": "Noah Jimenez"
        },
        {
            "price": 0,
            "stocks": 0
        }
    );

    db.inventory.find(
        {
            "price": {
                $lte: 5000
            }
        },
        {
           "_id": 0,
           "name": 1,
           "author": 1
        }
    );

// Evaluation Query Operator
/*
    $regex operator (regular expression) yung may author na may uppercase 'M' lang ang magsho2w

    Syntax: 
        db.collectionName.find({field: { $regex: 'pattern', $options: 'optionsValue'}});
*/
// Case Sensitive
    db.inventory.find(
        {
            "author": {
                $regex: 'M'
            }
        }
    );

// Case Insensitive ($options: $i) di'disregard ang case sensitivity

    db.inventory.find({

            "author": {
                $regex: 'M',
                $options: '$i'
            }
        });

// Can query operators be used on operations such as updateOne, updateMany, deleteOne, deleteMany??
/*
        Yes!.
        updateOne({criteria/query}, {$set});
        updateMany({criteria/query}, {$set});
        deleteOne({criteria/query});
        deleteMany({criteria/query});
*/